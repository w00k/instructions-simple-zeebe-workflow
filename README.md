# Zeebe Example with Microservices

## Introduction
This example run makes the call from zeebe-client to microservices, through the layers:

1. [zeebe-client](https://gitlab.com/w00k/zeebe-hello-world-client.git)
1. zeebe-api-gateway/zeebe-broker (use docker)
1. [zeebe-worker](https://gitlab.com/w00k/zeebe-worker-spring-boot.git)
1. [microservices](https://gitlab.com/w00k/hello-world-rest-api.git)

For run all the example you need to get all the repositories 

## Running 
The order to execute is: 

### run zeebe-api-gateway/zeebe-broker
Run zeebe with docker
```bash
$ docker run -p 26500-26502:26500-26502 --network host camunda/zeebe:latest
```

### run microservices
Follow the instructions from [here](https://gitlab.com/w00k/hello-world-rest-api)
or in Linux run 
```bash
$ ./helloworld-rest-api
```
### run work zeebe-worker-spring-boot-example
Clone the repository from [here](https://gitlab.com/w00k/zeebe-worker-spring-boot) and install with maven
```
$ mvn clean install
$ mvn compile
$ java -jar target/demo-0.0.1-SNAPSHOT.jar
```
### run zeebe-hello-world-client 
Clone the repository from [here](https://gitlab.com/w00k/zeebe-hello-world-client.git), then you need edit the file hello-world-process.bpmn, in url key you will need change the ip for you public ip

```xml
<!-- in the line 12 -->
<zeebe:header key="url" value="http://192.168.43.249:5000/hello-world"/>
```
And finally run it in Linux 
```
$ ./zeebe-hello-world-client
```

or build on Windows

## Testing
Now you can icheck this url [http://localhost:5001/hello-world-workflow/Olympia](http://localhost:5001/hello-world-workflow/Olympia) in you browser or use you favorite app to test APIs

![test.png](https://gitlab.com/w00k/instructions-simple-zeebe-workflow/-/raw/master/img/test.png)

Thanks.
